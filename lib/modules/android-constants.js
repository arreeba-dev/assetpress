module.exports.resizeFilter = 'Box';

module.exports.densities = {
  ldpi: 0.75,
  mdpi: 1,
  hdpi: 1.5,
  xhdpi: 2,
  xxhdpi: 3,
  xxxhdpi: 4
};

module.exports.allowedExtensions = ['.png', '.jpg', '.jpeg', '.gif'];
