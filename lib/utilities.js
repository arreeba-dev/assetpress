var fs, path, walk;

path = require('path');

fs = require('fs-extra');

walk = require('walkdir');

module.exports.resolvePath = function(passedFrom, passedString) {
  var from, ref, ref1, string, tilded;
  if (passedFrom == null) {
    passedFrom = '';
  }
  if (passedString == null) {
    passedString = false;
  }
  if (passedString) {
    ref = [passedFrom, passedString], from = ref[0], string = ref[1];
  } else {
    ref1 = [false, passedFrom], from = ref1[0], string = ref1[1];
  }
  tilded = string.substring(0, 1) === '~' ? process.env.HOME + string.substring(1) : string;
  if (from) {
    return path.resolve(from, tilded);
  } else {
    return path.resolve(tilded);
  }
};

module.exports.escapeShell = function(cmd) {
  return cmd.replace(/(["\s'$`\\])/g, '\\$1');
};

module.exports.addTrailingSlash = function(str) {
  if (str.slice(-1) !== '/') {
    return str + '/';
  } else {
    return str;
  }
};

module.exports.removeTrailingSlash = function(str) {
  return str.replace(/\/$/, "");
};

module.exports.cleanMove = function(from, to) {
  if (fs.existsSync(to)) {
    fs.removeSync(to);
  }
  fs.mkdirpSync(path.resolve(to, '..'));
  return fs.renameSync(from, to);
};

module.exports.dirtyMove = function(from, to) {
  var directoryPath, filepath, fromPath, i, len, paths, results, toPath, toPathDir;
  paths = walk.sync(from);
  results = [];
  for (i = 0, len = paths.length; i < len; i++) {
    directoryPath = paths[i];
    filepath = path.relative(from, directoryPath);
    fromPath = path.resolve(from, filepath);
    if (!fs.lstatSync(fromPath).isFile()) {
      continue;
    }
    toPath = path.resolve(to, filepath);
    toPathDir = path.parse(toPath).dir;
    fs.mkdirpSync(toPathDir);
    results.push(fs.renameSync(fromPath, toPath));
  }
  return results;
};

module.exports.move = function(from, to, clean) {
  var moveFunction;
  moveFunction = clean ? module.exports.cleanMove : module.exports.dirtyMove;
  return moveFunction(from, to);
};
